class Product {
  final String? title, image;

  Product({this.title, this.image});
}

List<Product> demo_products = [
  Product(title: "BTS-2cool4skool", image: "assets/images/cool.jpg"),
  Product(title: "BTS-Map of the soul 7", image: "assets/images/soul7.jpg"),
  Product(title: "BTS-Map of the soul Persona", image: "assets/images/persona.jpg"),
  Product(title: "BTS-Love yourself Tear", image: "assets/images/tear.jpg"),
  Product(title: "BTS-be", image: "assets/images/be.jpg"),
];
